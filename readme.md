## Outliers

# How can you start the project?

1. Download the project. 
2. Open the project via terminal (Mac, Linux) or CMD (Windows).
    cd outliers
3. Firstly you have to create the environment. To create the environment, you need
    install Anaconda. This is the web link to the Anaconda: https://www.anaconda.com/
4. Creating the environment in the terminal or CMD
    conda create -f outliers_environment.yml
5. You have to activate the environment
    Mac or Linux: source activate outliers
    Windows: activate outliers
    
    If you want to deactivate the environment the commands are:
    Mac or Linux: source deactivate outliers
    Windows: deactivate outliers
6. Start the project:
    Just type in terminal or CMD the following: python main.pyc
    
    